import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { XMLWriterComponent } from './xml/xmlwriter/xmlwriter.component';
import { XMLReaderComponent } from './xml/xmlreader/xmlreader.component';
import { NavbarComponent } from './navbar/navbar.component';

import { EmployeeService } from './services/employee.service';
import { HttpService } from './services/http.service';
import { LocationService } from './services/location.service';

import { TimerDemoComponent } from './timer-demo/timer-demo.component';
import { TimerComponent } from './timer-demo/timer/timer.component';
import { TimerService } from './timer-demo/timer/timer.service';
import { DecimalPipe } from '@angular/common';
import { MinsecPipe } from './timer-demo/timer/minsec.pipe';
import { AuthenticationService } from './timer-demo/authentication.service';


@NgModule({
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
    declarations: [
        AppComponent,
        XMLWriterComponent,
        XMLReaderComponent,
        NavbarComponent,
        TimerDemoComponent,
        TimerComponent,
        MinsecPipe
    ],
    providers: [
        EmployeeService,
        HttpService,
        LocationService,
        TimerService,
        DecimalPipe,
        AuthenticationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
