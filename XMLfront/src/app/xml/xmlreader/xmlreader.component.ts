import { Component, OnInit } from '@angular/core';
import { Employee } from '../../model/employee.model';
import { EmployeeService } from '../../services/employee.service';

@Component({
    selector: 'app-xmlreader',
    templateUrl: './xmlreader.component.html',
    styleUrls: ['./xmlreader.component.css']
})
export class XMLReaderComponent implements OnInit {
    employees: Employee[] = [];

    constructor(private employeeService: EmployeeService) {
    }

    ngOnInit() {
        this.getAllEmployees();
    }

    getAllEmployees() {
        this.employeeService.getAll()
            .subscribe((employees) => {
                this.employees = employees;
            });
    }

    filterByPosition(position: string) {
        this.filter("position", position);
    }

    filterByBuilding(building: string) {
        this.filter("building", building);
    }

    private filter(filterBy: string, filter: string) {
        if (filter === '') {
            return;
        }
        this.employeeService.get(filterBy, filter)
            .subscribe((employees) => {
                this.employees = employees;
            });
    }

    refresh() {
        this.getAllEmployees();
    }

}
