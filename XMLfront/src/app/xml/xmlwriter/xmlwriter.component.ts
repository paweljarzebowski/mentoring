import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { EmployeeService } from '../../services/employee.service';
import { LocationService } from '../../services/location.service';
import { Address } from '../../model/address.model';
import { Employee } from '../../model/employee.model';

@Component({
    selector: 'app-xmlwriter',
    templateUrl: './xmlwriter.component.html',
    styleUrls: ['./xmlwriter.component.css']
})
export class XMLWriterComponent implements OnInit {
    employee: Employee = new Employee();
    locations: Address[] = [];
    countries: string[] = [];
    locationDetailsVisible = false;

    constructor(private employeeService: EmployeeService,
                private locationService: LocationService,
                private router: Router) {
    }

    ngOnInit() {
        this.locationService.getAll()
            .subscribe(locations => this.locations = locations);
        this.locationService.getCountries()
            .subscribe(countries => this.countries = countries);
    }

    createEmployee() {
        this.employeeService.create(this.employee)
            .subscribe(next => this.router.navigate(['xml-reader']));
    }

    clearLocationDetails() {
        this.employee.location = new Address();
    }

}
