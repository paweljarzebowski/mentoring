import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { XMLReaderComponent } from './xml/xmlreader/xmlreader.component';
import { XMLWriterComponent } from './xml/xmlwriter/xmlwriter.component';
import { TimerDemoComponent } from './timer-demo/timer-demo.component';


const routes: Routes = [
    {path: 'timer-demo', component: TimerDemoComponent},
    {path: 'xml-writer', component: XMLWriterComponent},
    {path: 'xml-reader', component: XMLReaderComponent},
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
