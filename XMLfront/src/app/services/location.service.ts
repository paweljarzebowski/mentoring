import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LocationService extends HttpService {

    constructor(http: HttpClient) {
        super(http);
        this.url = 'http://localhost:8080/location';
    }

    getCountries(): Observable<any> {
        return this.http.get(this.url + '/countries');
    }

}
