import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService extends HttpService {

    constructor(http: HttpClient) {
        super(http);
        this.url = 'http://localhost:8080/employee';
    }

}
