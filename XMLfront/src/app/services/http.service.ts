import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {
    public url: string;

    constructor(public http: HttpClient) {
    }

    getAll(): Observable<any> {
        return this.http.get(this.url);
    }

    get(filterBy, filter): Observable<any> {
        return this.http.get(this.url + '/' + filterBy + '/' + filter);
    }

    create(resource): Observable<any> {
        return this.http.post(this.url, resource);
    }

}
