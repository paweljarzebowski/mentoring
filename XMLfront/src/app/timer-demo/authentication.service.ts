export class AuthenticationService {
    private isAuthenticated: boolean = false;

    getIsAuthenticated(): boolean {
        return this.isAuthenticated;
    }

    setIsAuthenticated(isAuthenticated: boolean): void {
        this.isAuthenticated = isAuthenticated;
    }
}