import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { TimerService } from './timer/timer.service';

@Component({
  selector: 'app-timer-demo',
  templateUrl: './timer-demo.component.html',
  styleUrls: ['./timer-demo.component.css']
})
export class TimerDemoComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService,
              private timerService: TimerService) { }

  ngOnInit() {
  }

  login() {
    this.timerService.subscribeToTimer();
  }

  logout() {
    this.timerService.unsubscribeFromTimer();
  }

}
