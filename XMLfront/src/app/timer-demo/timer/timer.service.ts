import {Injectable, Injector} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import 'rxjs/Rx';
import { AuthenticationService } from '../authentication.service';

@Injectable()
export class TimerService {
    public static TIME_OUT: number = 20;
    private timerSubscription: Subscription = new Subscription();
    private timer: Observable<number>;
    private time: number;
    // private authService: AuthenticationService;

    // workaround circular dependencies by injecting Injector instead of AuthenticationService
    constructor(private authenticationService: AuthenticationService) {
        // setTimeout(() => this.authService = injector.get(AuthenticationService));
        this.timerSubscription.unsubscribe();
        this.timer = Observable.timer(0, 1000).map((t) => TimerService.TIME_OUT - t);
    }

    public subscribeToTimer(): void {
        console.log('subscribe to Timer');
        if (!this.timerSubscription.closed) {
            console.log('already subscribed!');
            return;
        }
        this.timerSubscription = this.timer
            .take(TimerService.TIME_OUT + 1)
            .subscribe(
                (time) => {
                    this.authenticationService.setIsAuthenticated(true);
                    this.time = time;
                    console.log(time);
                },
                () => {
                },
                () => {
                    this.authenticationService.setIsAuthenticated(false);
                    console.log('complete');
                }
            );
    }

    public unsubscribeFromTimer() {
        console.log('unsubscribe from Timer');
        this.authenticationService.setIsAuthenticated(false);
        this.timerSubscription.unsubscribe();
    }
}
