import {Pipe, PipeTransform} from "@angular/core";
import {DecimalPipe} from "@angular/common";

@Pipe({
    name: 'minsec',
})
export class MinsecPipe implements PipeTransform {

    constructor(private decPipe: DecimalPipe) {
    }

    transform(time: number): string {
        const minutes: number = Math.floor(time / 60);
        const seconds = this.decPipe.transform(time - (minutes * 60), '2.0-0');
        return minutes + ':' + seconds;
    }

}
