export class Address {

    constructor(public country: string = '',
                public city: string = '',
                public postCode: string = '',
                public street: string = '',
                public building: string = '') {
    }

}
