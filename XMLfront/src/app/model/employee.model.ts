import { Address } from './address.model';

export class Employee {

    constructor(public id: string = '',
                public name: string = '',
                public position: string = '',
                public email: string = '',
                public location: Address = new Address()) {
    }

}
