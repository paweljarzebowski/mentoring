package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"config", "controller", "filter", "xml"})
public class XMLApplication {

    public static void main(String[] args) {
        SpringApplication.run(XMLApplication.class, args);
    }

}
