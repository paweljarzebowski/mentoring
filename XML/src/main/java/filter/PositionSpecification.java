package filter;

import xml.Employee;

public class PositionSpecification implements Specification<Employee> {
	
	private String position;

	public PositionSpecification(String position) {
		this.position = position;
	}

	@Override
	public boolean isSatisfied(Employee employee) {
		return employee.getPosition().equalsIgnoreCase(position);
	}

	@Override
	public String toString() {
		return "Position: " + position;
	}
}
