package filter;

import xml.Employee;

public class BuildingSpecification implements Specification<Employee> {
	
	private String building;

	public BuildingSpecification(String building) {
		this.building = building;
	}

	@Override
	public boolean isSatisfied(Employee employee) {
		return employee.getLocation().getBuilding().equalsIgnoreCase(building);
	}

	@Override
	public String toString() {
		return "Building: " + building;
	}

}
