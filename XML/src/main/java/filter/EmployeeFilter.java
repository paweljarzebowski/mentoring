package filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import xml.Employee;

@Component
public class EmployeeFilter implements Filter<Employee> {

	private static final Log log = LogFactory.getLog(EmployeeFilter.class);

	@Override
	public Stream<Employee> filter(List<Employee> employees, Specification<Employee> spec) {
		log.info("EmployeeRegister filtered by " + spec);
		return employees.stream()
				.filter(employee -> spec.isSatisfied(employee));
	}

}
