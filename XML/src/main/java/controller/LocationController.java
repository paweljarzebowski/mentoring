package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xml.Address;

import java.util.List;

import static xml.Locations.COUNTRIES;
import static xml.Locations.LOCATIONS;

@RestController
@RequestMapping("/location")
public class LocationController {

    @GetMapping
    public List<Address> getLocations() {
        return LOCATIONS;
    }

    @GetMapping(value = "/countries")
    public List<String> getCountries() {
        return COUNTRIES;
    }

}
