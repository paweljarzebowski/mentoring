package controller;

import filter.BuildingSpecification;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import filter.EmployeeFilter;
import filter.PositionSpecification;
import filter.Specification;
import xml.Employee;
import xml.EmployeeRegister;

import java.util.List;
import java.util.stream.Collectors;

import static xml.EmployeeRegister.PATHNAME;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private static final Log log = LogFactory.getLog(EmployeeController.class);

    @Autowired
    private EmployeeRegister employeeRegister;

    @Autowired
    private EmployeeFilter employeeFilter;

    @GetMapping
    public List<Employee> getAllEmployees() {
        return employeeRegister.unmarshalFromFile(PATHNAME).getEmployees();
    }

    @GetMapping(value = "/position/{position}")
    public List<Employee> filterEmployeesByPosition(@PathVariable("position") String position) {
        return filter(new PositionSpecification(position));
    }

    @GetMapping(value = "/building/{building}")
    public List<Employee> filterEmployeesByBuilding(@PathVariable("building") String building) {
        return filter(new BuildingSpecification(building));
    }

    @PostMapping
    public void addEmployee(@RequestBody Employee employee) {
        employeeRegister = employeeRegister.unmarshalFromFile(PATHNAME);
        employeeRegister.getEmployees().add(employee);
        log.info("Employee '" + employee.getName() + "' added to register");
        employeeRegister.marshalToFile(PATHNAME);
    }

    private List<Employee> filter(Specification<Employee> spec) {
        List<Employee> employees = getAllEmployees();
        return employeeFilter
                .filter(employees, spec)
                .collect(Collectors.toList());
    }

}
