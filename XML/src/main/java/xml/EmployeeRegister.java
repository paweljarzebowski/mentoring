package xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EmployeeRegister {

    private static final String DIR_NAME = "employee-register";
    private static final String FILE_NAME = "employee-register.xml";
    public static final String PATHNAME = DIR_NAME + "\\" + FILE_NAME;

    private List<Employee> employees = new ArrayList<>();

    public EmployeeRegister() {
    }

    private static final Log log = LogFactory.getLog(EmployeeRegister.class);

    @PostConstruct
    public void postConstruct() {
        log.info("EmployeeRegister constructed");
        createRegisterDirectory();
    }

    public void marshalToFile(String pathname) {
        File file = new File(pathname);
        if (!file.exists()) {
            createRegisterDirectory();
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(EmployeeRegister.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(this, file);
            log.info("EmployeeRegister saved to %WorkingDirectory%\\" + pathname);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void marshalToConsole() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(EmployeeRegister.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(this, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public EmployeeRegister unmarshalFromFile(String pathname) {
        File file = new File(pathname);
        EmployeeRegister employeeRegister = new EmployeeRegister();
        if (!file.exists()) {
            return employeeRegister;
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(EmployeeRegister.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            employeeRegister = (EmployeeRegister) unmarshaller.unmarshal(file);
            log.info("EmployeeRegister retrieved from %WorkingDirectory%\\" + pathname);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return employeeRegister;
    }
    
    public List<Employee> getEmployees() {
        return employees;
    }

    private void createRegisterDirectory() {
        boolean dirCreated = new File(DIR_NAME).mkdir();
        if (dirCreated) {
            String workingDir = System.getProperty("user.dir");
            log.info("Directory  " + workingDir + "\\" + DIR_NAME + " created");
        }
    }

    @Override
    public String toString() {
        return "EmployeeRegister{\n" +
                "\temployees=" + employees +
                "\n}";
    }

}
