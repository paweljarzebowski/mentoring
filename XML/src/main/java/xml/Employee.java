package xml;

import javax.xml.bind.annotation.XmlType;

@XmlType
public class Employee {

    private String id;
    private String name;
    private String position;
    private String email;
    private Address location;

    public Employee() {
    }

    public Employee(String id, String name, String position, String email, Address location) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.email = email;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "\n\t\tEmployee{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", email='" + email + '\'' +
                ", location=" + location +
                "}";
    }

}
