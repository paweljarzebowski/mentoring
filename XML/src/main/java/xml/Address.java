package xml;

import javax.xml.bind.annotation.XmlType;

@XmlType
public class Address {

    private String country;
    private String city;
    private String postCode;
    private String street;
    private String building;

    public Address() {
    }

    public Address(String country, String city, String postCode, String street, String building) {
        this.country = country;
        this.city = city;
        this.postCode = postCode;
        this.street = street;
        this.building = building;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    @Override
    public String toString() {
        return "\n\t\t\tAddress{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", postCode='" + postCode + '\'' +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                '}';
    }

}
