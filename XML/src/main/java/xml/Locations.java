package xml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Locations {

    public static final List<Address> LOCATIONS = new ArrayList<>();
    public static final List<String> COUNTRIES;

    static {
        LOCATIONS.add(new Address("Poland", "Gdynia", "81-537", "Łużycka", "8A"));
        LOCATIONS.add(new Address("Poland", "Gdynia", "81-537", "Łużycka", "8B"));
        LOCATIONS.add(new Address("Poland", "Gdynia", "81-537", "Łużycka", "8C"));

        COUNTRIES = Arrays.asList("Poland", "Denmark", "Finland", "Norway", "Sweden");
    }

}
